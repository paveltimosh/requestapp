package com.timoshuk.requestapp.model;

import com.timoshuk.requestapp.model.enums.Role;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

@Entity
@Builder
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@Table(name = "users", schema = "public")
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity implements Serializable {

    private static final long serialVersionUID = -3961240478030776048L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "users_id_seq", allocationSize = 0)
    private Long id;

    @Column
    private String login;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

}
