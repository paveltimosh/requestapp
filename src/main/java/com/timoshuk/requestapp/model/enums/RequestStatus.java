package com.timoshuk.requestapp.model.enums;

public enum  RequestStatus {
    OPEN,
    DONE,
    REJECTED
}
