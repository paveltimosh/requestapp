package com.timoshuk.requestapp.model.enums;

public enum Role {
    USER,
    ADMIN
}
