package com.timoshuk.requestapp.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.timoshuk.requestapp.model.enums.RequestStatus;
import com.timoshuk.requestapp.util.LocalDateAttributeConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Builder
@Data
@Table(name = "requests", schema = "public")
@NoArgsConstructor
@AllArgsConstructor
public class Request implements Serializable {

    private static final long serialVersionUID = 5661028589496728458L;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "request_seq")
    @SequenceGenerator(name = "request_seq", sequenceName = "requests_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "id_of_user")
    private Long createdUserId;

    @Column(name = "descr")
    private String descr;

    @Column(name = "due_date", updatable = false)
    @Convert(converter = LocalDateAttributeConverter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dueDate;

    @Column(name = "request_status")
    @Enumerated(EnumType.STRING)
    private RequestStatus requestStatus;
}
