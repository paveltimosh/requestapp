package com.timoshuk.requestapp.controller;

import com.timoshuk.requestapp.dto.UserDTO;
import com.timoshuk.requestapp.model.UserEntity;
import com.timoshuk.requestapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO get(@PathVariable Long id) {
        return userService.findById(id);
    }

    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public UserEntity signUp(@Valid @RequestBody UserDTO userDTO){
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        return userService.create(userDTO);
    }

    @GetMapping(params = {"page", "size"}, path = "/paginated")
    public List<UserDTO> getUsersPaginated(@RequestParam ("page") int page,
                                              @RequestParam ("size") int size){
        Pageable paginatedAndSortedByLogin = PageRequest.of(page, size, Sort.by("login").descending());
        return userService.findAllUsersPaginated(paginatedAndSortedByLogin);
    }
}
