package com.timoshuk.requestapp.exceptions;

public class AccesDenied  extends RuntimeException{
    public AccesDenied() {
        super();
    }

    public AccesDenied(String message) {
        super(message);
    }

    public AccesDenied(String message, Throwable cause) {
        super(message, cause);
    }
}
