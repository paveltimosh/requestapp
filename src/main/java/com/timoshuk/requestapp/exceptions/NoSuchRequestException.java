package com.timoshuk.requestapp.exceptions;


public class NoSuchRequestException extends RuntimeException {
    public NoSuchRequestException() {
        super();
    }

    public NoSuchRequestException(String message) {
        super(message);
    }

    public NoSuchRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
