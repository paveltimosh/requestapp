package com.timoshuk.requestapp.service;

import com.timoshuk.requestapp.dto.UserDTO;
import com.timoshuk.requestapp.model.UserEntity;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface UserService {
    UserEntity create(UserDTO userDTO);
    UserDTO findById(Long id);
    UserDTO findByLogin(String login);
    List<UserDTO> findAllUsersPaginated(Pageable pageable);
}
