package com.timoshuk.requestapp.service.impl;

import com.timoshuk.requestapp.DAO.UserRepo;
import com.timoshuk.requestapp.dto.UserDTO;
import com.timoshuk.requestapp.dto.mapper.UserMapper;
import com.timoshuk.requestapp.exceptions.NoSuchUserEsception;
import com.timoshuk.requestapp.model.UserEntity;
import com.timoshuk.requestapp.model.enums.Role;
import com.timoshuk.requestapp.service.UserService;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserMapper userMapper;


    @Override
    public UserEntity create(UserDTO userDTO) {
        if (userDTO.getRole() == null || userDTO.getRole().equals(Role.ADMIN)){
            userDTO.setRole(Role.USER);
        }
        UserEntity user = userMapper.convertToEntity(userDTO);
        return userRepo.save(user);
    }

    @Override
    public UserDTO findById(Long id) {
        UserEntity user = userRepo.findById(id).orElseThrow(
                ()-> new NoSuchUserEsception("User with this ID:"+ id + " does not exist")
        );
        return userMapper.convertToDTO(user);
    }

    @Override
    public UserDTO findByLogin(String login) {
        UserEntity user = userRepo.findByLogin(login).orElseThrow(
                () -> new NoSuchUserEsception("User with this login: " + login + " does not created")
        );
        return userMapper.convertToDTO(user);
    }

    @Override
    public List<UserDTO> findAllUsersPaginated(Pageable pageable){
        return userRepo.findAll(pageable).getContent().stream()
                .map(userEntity -> userMapper.convertToDTO(userEntity))
                .collect(Collectors.toList());
    }

}
