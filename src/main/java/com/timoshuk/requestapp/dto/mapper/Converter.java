package com.timoshuk.requestapp.dto.mapper;

public interface Converter<S,D> {
    S convertToEntity(D destination);
    D convertToDTO(S source);
}
