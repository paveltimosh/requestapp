package com.timoshuk.requestapp.dto.mapper;

import com.timoshuk.requestapp.dto.UserDTO;
import com.timoshuk.requestapp.model.UserEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class UserMapper implements Converter<UserEntity,UserDTO> {

    private ModelMapper modelMapper;

    public UserMapper() {
        modelMapper = new ModelMapper();
    }

    @Override
    public UserEntity convertToEntity(UserDTO destination) {
        return modelMapper.map(destination, UserEntity.class);
    }

    @Override
    public UserDTO convertToDTO(UserEntity source) {
        return modelMapper.map(source, UserDTO.class);
    }
}
