package com.timoshuk.requestapp.dto;

import com.timoshuk.requestapp.model.enums.Role;
import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id;

    @NotEmpty(message = "This field cannot be empty!")
    @Size(min = 3, max = 25, message = "Login cannot be longer than 30 characters or less than 3 characters!")
    private String login;

    @NotEmpty(message = "This field cannot be empty!")
    private String password;

    private Role role;
}
