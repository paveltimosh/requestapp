package com.timoshuk.requestapp.security;

import com.timoshuk.requestapp.dto.UserDTO;
import com.timoshuk.requestapp.model.UserEntity;
import com.timoshuk.requestapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Autowired
    private UserService userService;

    @Override
    public User loadUserByUsername(final String username) throws UsernameNotFoundException {
        UserDTO userDTO = null;
        try {
            userDTO = userService.findByLogin(username);
            Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
            grantedAuthoritiesList.add(new SimpleGrantedAuthority(String.valueOf(userDTO.getRole())));
            User user = new User(userDTO.getLogin(), userDTO.getPassword(), grantedAuthoritiesList);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
    }
}
