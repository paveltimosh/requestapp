package com.timoshuk.requestapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private String clientid = "userapp";
    private String clientSecret = "8247861";
    private String privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAovZHdVs/8wyNuyzKOcfdbbk9614Tzx3DqQqMv2f/NxNkzUWk\n" +
            "aMuFRVRNi/Kirw8Tt2InKCtqgJS/mj6WT0RaJKzA+Qk3GfdLS3EeiezQcypMOpIo\n" +
            "buZ0HAC5dh3KuWikhvKgknw1SmTf/XRJij3b2oXwRwN7kup0FrhtiBgaqFe34QDs\n" +
            "iH+mBePJmfINbE5n47CbnqdcEVCTS8JbjTi0qv3t7qLC0x8JDWHxpW0eExeCzDEV\n" +
            "KIpckqJZ8QvWdam7EwV9QQrhzu7AjVPms7iREp15Qp9j4LQzSs34Mm3pl2OXHZwU\n" +
            "rRAaQaHz1SICGjk+i/L73q2IA6i6KAASIuXfnQIDAQABAoIBAB6nhTmmOJlXuzGC\n" +
            "c2f7GMQTHHqx77f+OVgLC/7VaUTK+SND/nFYF3G7MfspYUDc7TCJ3iPIRvh5ArHZ\n" +
            "JB01Fl/fv00ZVqwjwJ4oYT5AtdV0F3LNG54A68Z4vGpjoEbQUGKyMVJc2wlrgFcF\n" +
            "EPBYQosQtuok1ZIFWGENwEzhe+I+kRPMbJzCjSORSw8uAfI1u8ucqgZUpYjdBPeN\n" +
            "WxjRG8XxsZc1A1wf71XRCu3eTcNnbeMOyt8+n5+t3kyNg+RFaqecqY2WBWaOKtGm\n" +
            "u9/a+ckEsGGoVJkpkzDATkFnzW0o0RXotVtC08s73sVSV1TOVVrvDTFATk9RbrPL\n" +
            "Z/TgrGECgYEA0MCOs/4G6jSVSLxR5R0zE+kxP+GySuNHimDbpMb6gr7g7wyc5mdD\n" +
            "xFzwZ/9gP7dsuhmqF63mymhcYyjYNjnaEUtLPjFLju27PbvaShbu66EmiezlCZlA\n" +
            "Yu4Zw+yhO3yHChsJQqw5vdgb6iaRs42gPsZoU4BwIdoO9jMLvoEWg2sCgYEAx9iQ\n" +
            "x4z7GmHeZZx7a5XiDlgETuJc+02L0irPu8vNzHBm7mrwiymA/XfJZx33sfeNL89W\n" +
            "BqggWuFlZ114Ogd0UPbr5M73lBJ3bY7TiYmKe/60CkgXQNSyhOSFtaYxYGvNIzbd\n" +
            "rN/wy2yv3CN+RMP3UFD+8XUNEMgXEMO8w0NFcxcCgYBg7wlUy0CqwIARpmDg4w40\n" +
            "0LPt1f/UhnVR7GgRu9PXoosQderlkk4FgKKhqBQpgiMax5AULsxccNl1siG0DGkW\n" +
            "XsAxwPI1DG9F21Uh0kRfZhVfNoGkU2Vyit2r/NVhqhGKv8MtEow9e3x63i4KElm2\n" +
            "zKrlG172WStZZ5qse4TYwQKBgQCWuUtYdYfc+ftV/5NGLSD1WITHSgp+rpnJxOgv\n" +
            "5yIsEopPPvB0xLS4dmO8Fen+m4osiN+E3cCpSl3Ee8Z1XbHobpKzWzV2xAa25oW8\n" +
            "sbN0mUEIkB6ZktGhUQlXWyTaZ/Orn3HJNIzToAFysoJFMteQB8aALZvREFf1GOuB\n" +
            "CXeTuQKBgBjFzO5nj6tDWBrzr/wdsZXHkV5D6xauGCV20ZLuTBdoAu0hqwj7y9Ko\n" +
            "Mxvu2lBA4BBek4teiUnmenaDCnlwfEDnBGdfCd7hi9wAeJ+1z70QVG6yLc12Ems+\n" +
            "gORtVjsq0TZ93Pzpa/+jKbBzeUIjgoiYHMMjqr0YBe0w4MW5JjUN\n" +
            "-----END RSA PRIVATE KEY-----";
    private String publicKey = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAovZHdVs/8wyNuyzKOcfd\n" +
            "bbk9614Tzx3DqQqMv2f/NxNkzUWkaMuFRVRNi/Kirw8Tt2InKCtqgJS/mj6WT0Ra\n" +
            "JKzA+Qk3GfdLS3EeiezQcypMOpIobuZ0HAC5dh3KuWikhvKgknw1SmTf/XRJij3b\n" +
            "2oXwRwN7kup0FrhtiBgaqFe34QDsiH+mBePJmfINbE5n47CbnqdcEVCTS8JbjTi0\n" +
            "qv3t7qLC0x8JDWHxpW0eExeCzDEVKIpckqJZ8QvWdam7EwV9QQrhzu7AjVPms7iR\n" +
            "Ep15Qp9j4LQzSs34Mm3pl2OXHZwUrRAaQaHz1SICGjk+i/L73q2IA6i6KAASIuXf\n" +
            "nQIDAQAB\n" +
            "-----END PUBLIC KEY-----";

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Bean
    public JwtAccessTokenConverter tokenEnhancer() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(privateKey);
        converter.setVerifierKey(publicKey);
        return converter;
    }
    @Bean
    public JwtTokenStore tokenStore() {
        return new JwtTokenStore(tokenEnhancer());
    }
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore())
                .accessTokenConverter(tokenEnhancer());
    }
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(clientid)
                .secret(passwordEncoder.encode(clientSecret))
                .scopes("read", "write")
                .authorizedGrantTypes("password", "refresh_token").accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(20000);

    }
}